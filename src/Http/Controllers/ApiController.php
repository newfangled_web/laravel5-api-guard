<?php

namespace Gholol\ApiGuard\Http\Controllers;

use Input;
use Config;
use League\Fractal\Manager;
use Illuminate\Foundation\Bus\DispatchesJobs;
use EllipseSynergie\ApiResponse\Laravel\Response;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;

abstract class ApiController extends BaseController
{
  use DispatchesJobs, ValidatesRequests;

  public function __construct(Manager $manager)
  {

    $this->middleware('apiguard');

    $manager->parseIncludes(Input::get(Config::get('apiguard.includeKeyword', 'include'), 'include'));

    $this->response = new Response($manager);
  }

}
