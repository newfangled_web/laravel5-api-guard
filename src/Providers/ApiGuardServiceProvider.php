<?php

namespace Gholol\ApiGuard\Providers;

use Illuminate\Support\ServiceProvider;

class ApiGuardServiceProvider extends ServiceProvider
{

  /**
   * Bootstrap the application services.
   *
   * @return void
   */
  public function boot()
  {

      $this->commands([
          'Gholol\ApiGuard\Console\Commands\GenerateApiKeyCommand',
          'Gholol\ApiGuard\Console\Commands\DeleteApiKeyCommand',
      ]);
  }

  /**
   * Register the application services.
   *
   * @return void
   */
  public function register()
  {
      //
  }

}
